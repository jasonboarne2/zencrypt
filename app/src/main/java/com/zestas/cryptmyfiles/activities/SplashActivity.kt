package com.zestas.cryptmyfiles.activities

import android.annotation.SuppressLint
import android.content.Intent
import android.net.Uri
import android.os.Build
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.os.Parcelable
import com.zestas.cryptmyfiles.constants.ZenCryptConstants

@SuppressLint("CustomSplashScreen")
class SplashActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        val toMainActivityIntent = Intent(this@SplashActivity, MainActivity::class.java)
        when (intent.action) {
            Intent.ACTION_SEND -> {
                val receivedUri = if (Build.VERSION.SDK_INT >= 33) {
                    intent.getParcelableExtra(Intent.EXTRA_STREAM, Parcelable::class.java) as? Uri
                } else {
                    intent.getParcelableExtra<Parcelable>(Intent.EXTRA_STREAM) as? Uri
                }
                if (receivedUri != null) {
                    toMainActivityIntent.putExtra(
                        ZenCryptConstants.REQUEST_CODE,
                        ZenCryptConstants.FROM_RECEIVED_URI
                    )
                    toMainActivityIntent.data = receivedUri
                    toMainActivityIntent.flags = Intent.FLAG_GRANT_READ_URI_PERMISSION
                }
            }
            Intent.ACTION_VIEW -> {
                val receivedUri = intent.data
                if (receivedUri != null) {
                    toMainActivityIntent.putExtra(
                        ZenCryptConstants.REQUEST_CODE,
                        ZenCryptConstants.FROM_RECEIVED_URI
                    )
                    toMainActivityIntent.data = receivedUri
                    toMainActivityIntent.flags = Intent.FLAG_GRANT_READ_URI_PERMISSION
                }
            }
            else -> {
                // Handle other intents, such as being started from the home screen
                // do nothing
            }
        }

        startActivity(toMainActivityIntent)
        finish()
    }
}