package com.zestas.cryptmyfiles.helpers.autodeletefiles

import android.content.Context
import androidx.lifecycle.*
import com.zestas.cryptmyfiles.constants.ZenCryptConstants
import java.io.File

class ZencryptLifecycleObserver(var context: Context) : LifecycleEventObserver {

    override fun onStateChanged(source: LifecycleOwner, event: Lifecycle.Event) {
        when (event) {
            Lifecycle.Event.ON_RESUME -> {}
            Lifecycle.Event.ON_PAUSE -> {}
            Lifecycle.Event.ON_DESTROY-> {
                val unencryptedDir = ZenCryptConstants.decryptedFilesDir(context)
                for (file: File in unencryptedDir.listFiles()!!) {
                    if (!file.isDirectory)
                        file.delete()
                }
            }
            Lifecycle.Event.ON_CREATE -> {}
            Lifecycle.Event.ON_START -> {}
            Lifecycle.Event.ON_STOP -> {
                val unencryptedDir = ZenCryptConstants.decryptedFilesDir(context)
                for (file: File in unencryptedDir.listFiles()!!) {
                    if (!file.isDirectory)
                        file.delete()
                }
            }
            Lifecycle.Event.ON_ANY -> {}
        }
    }
}

